
import java.util.Scanner; // Import der Klasse Scanner
public class Konsoleneingabe 
{
 
 public static void main(String[] args) // Hier startet das Programm
 {
	 

 Scanner myScanner = new Scanner(System.in); 
 int ergebnis;
 ergebnis=0;
 
 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");  
 int zahl1 = myScanner.nextInt(); 
 
 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
 int zahl2 = myScanner.nextInt();
 
 System.out.print("Welche rechenart wollen sie nehemen:");
 char rechenart = myScanner.next().charAt(0);
 
 
 if(rechenart==('+')) {
 	ergebnis = zahl1 + zahl2;
 	}
 if(rechenart=='-') {
	 ergebnis = zahl1 - zahl2;
 }
 if(rechenart=='/') {
	 ergebnis = zahl1 / zahl2;
 }
 if(rechenart=='*') {
	 ergebnis = zahl1 * zahl2;
 }
 System.out.print("\n\n\nErgebnis der rechnung lautet: ");
 System.out.print( ergebnis ); 
 
 
 //Aufgabe 2
 System.out.print("\nHallo User, Bitte geben sie ihren Vornamen ein: ");
 String name = myScanner.next();
 
 
 System.out.print("Bitte geben  sie ihr Alter ein: ");
 int alter = myScanner.nextInt();
 
 System.out.println("\n\nUser:"+ name + ", " + alter + " Jahre alt");
 
 myScanner.close();
 
 } 
}
