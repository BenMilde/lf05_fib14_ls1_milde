
public class Konfigurationschaos {
		public static void main(String[] args) {
			// Mit Ben Milde gemeinsam gemacht
			int cent, muenzenCent, muenzenEuro, euro, summe;
			double prozent, patrone, maximum;
			String typ, bezeichnung, name;
			char sprachModul;
			boolean status;
			final byte PRUEFNR;

			typ = "Automat AVR";
			bezeichnung = "Q2021_FAB_A";
			name = typ + " " + bezeichnung;
			sprachModul = 'd';
			PRUEFNR = 4;
			maximum = 100.00 ;
			patrone = 46.24;
			muenzenEuro = 130;
			muenzenCent = 1280;
			prozent = maximum - patrone;
			summe = muenzenCent + muenzenEuro * 100;
			euro = summe / 100;
			cent = summe % 100;
			status = (euro <= 150) && (prozent >= 50.00) && (!(PRUEFNR == 5 || PRUEFNR == 6)) && (euro >=50) && (cent != 0) && (sprachModul == 'd');;
			
			
			System.out.println("Name: " + name);
			System.out.println("Sprache: " + sprachModul);
			System.out.println("Pr�fnummer: " + PRUEFNR);
			System.out.println("F�llstand Patrone: " + prozent + " %");
			System.out.println("Summe Euro: " + euro + " Euro");
			System.out.println("Summe Rest: " + cent + " Cent");
			System.out.println("Status: " + status);
}
}