import java.util.Scanner;
public class Fahrkartenauromat_Methoden {

	public static void main(String[] args) {
		
		
		while(true) {
			System.out.printf("Ihr Restgeld beträgt: %.2f", fahrkartenBezahlen(fahrkartenbestellungErfassen()));
			fahrkartenAusgeben();
		}

	}
	
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double ticketPreis = 1.50;
		if(ticketPreis < 0) {
			System.out.println("Error: Der Ticketpreis darf kein negativen Wert bertragen. Der Wert wurde auf '1,50€' zurückgesetzt");
			ticketPreis = 1.50;
		}
		int ticketNr;
		double zuZahlenderBetrag = 0;
		int ticketTyp = 0;
		
		while(ticketTyp == 0 || ticketPreis == 1.50) {
		System.out.println("Bitte wählen sie den Tickettarif aus  Sie können wählen zwischen:" + "Einzelfahrschein Regeltarif AB: [2,90€] (1)\n" + "Tageskarte Regeltarif AB [8,60€] (2)" + "Kleingruppe-Tageskarte AB [23,50€] (3)\n");
		System.out.println("Ihre Wahl:");
		ticketTyp = tastatur.nextInt();
		switch(ticketTyp) {
			case 1:
				ticketPreis = 2.90;
				break;
			case 2:
				ticketPreis = 8.60;
				break;
			case 3:
				ticketPreis = 23.50;
				break;
			default:
				ticketTyp = 0;
				ticketPreis = 1.50;
				System.out.println("Fehler: Sie haben ein ungültiges Ticket ausgewählt, bitte w�hlen erneut aus.");
				
				
		}
		
		}
		System.out.println("Ticketpreis: " + ticketPreis + "€");
		System.out.println("Anzahl der Tickets?:");
		ticketNr = tastatur.nextInt();
		if(ticketNr > 10) {
			System.out.println("Fehler: max 10 Tickets gleichzeitig kaufen. Der Wert wurde auf '1' gesetzt.");
			ticketNr = 1;
		}
		
		double arr = zuZahlenderBetrag = ticketPreis * ticketNr;
		
	
		return arr;
		
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		double rückgabebetrag, eingezahlterGesamtbetrag = 0, zuZahlenderBetrag = zuZahlen, eingeworfeneMuenze;
		Scanner tastatur = new Scanner(System.in);
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMuenze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMuenze;
	       }
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0) {
	    	   rückgeldAusgeben(rückgabebetrag);
	    	   
	       }
	       return rückgabebetrag;
		
	}
	public static boolean rückgeldAusgeben(double rückgabebetrag) {

	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in Höhe von %.2f EURO ", rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println(muenzeAusgabe(2, "Euro"));
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(1, "Euro"));
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(50, "Cent"));
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(20, "Cent"));
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(10, "Cent"));
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println(muenzeAusgabe(5, "Cent"));
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	       return true;
	       
	}
	public static void fahrkartenAusgeben() {

			System.out.println("\nFahrschein wird ausgegeben");
		       
		}
	
	public static String muenzeAusgabe(int betrag, String einheit) {
		
		String s = betrag + " " + einheit;
		return s;
		
		
	}
}

