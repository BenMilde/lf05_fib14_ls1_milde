class Konsolenausgabe2{
	public static void main(String[] args){
//Aufgabe 1		
		System.out.printf( "%4s\n", "**");
		System.out.printf( "%-4s", "*");
		System.out.printf( "%2s\n", "*");
		System.out.printf( "%-4s", "*");
		System.out.printf( "%2s\n", "*");
		System.out.printf( "%4s\n", "**");
		
//Aufgabe 2
		
		System.out.printf( "|%-5s %1s %11s %s \n",    "0!","=","=", 1);
		System.out.printf( "|%-5s %3s %9s %s\n",    "1!","= 1","=", 1);
		System.out.printf( "|%-5s %5s %7s %s\n",    "2!","= 1*2","=", 2);
		System.out.printf( "|%-5s %7s %5s %s\n",    "3!","= 1*2*3","=", 6);
		System.out.printf( "|%-5s %9s %3s %s\n",    "4!","= 1*2*3*4","=", 24);
		System.out.printf( "|%-5s %10s %1s %s\n",    "5!","= 1*2*3*4*5","=", 120);
		
//Aufgabe 3
		System.out.printf( "|%-12s %s %10s \n",    "Fahrenheit","|","Celsius");
		System.out.printf( "|%-12s %s %10s \n",    "|%.2f|\n",-20, "|", "|%.2f|\n",-28.8889);
		System.out.printf( "|%-12s %s %10s \n",    "|%.2f|\n",-10, "|", "|%.2f|\n",-23.3333);
		System.out.printf( "|%-12s %s %10s \n",    "|%.2f|\n",0, "|", "|%.2f|\n",-17.7778);
		System.out.printf( "|%-12s %s %10s \n",    "|%.2f|\n",20, "|", "|%.2f|\n",-6.6667);
		System.out.printf( "|%-12s %s %10s \n",    "|%.2f|\n",30, "|", "|%.2f|\n",-1.1111);
	
	}
}