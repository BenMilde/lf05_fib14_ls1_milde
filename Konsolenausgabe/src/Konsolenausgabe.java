
class Konsolenausgabe{
	public static void main(String[] args) {

		System.out.println("HelloBerlin");
		System.out.print("BerlinHello");
		
		System.out.println("\"HelloBerlin\"");
		System.out.println("Hello Berlin" + " Du bist Mega");
		//Der Befehl print lässt die Strings in der Konsole anzeigen, println macht das gleich bloß mit zeilenumbruch

// Aufgabe 2
		
		System.out.println("       *");
		System.out.println("      ***");
		System.out.println("     *****");
		System.out.println("    *******");
		System.out.println("   *********");
		System.out.println("  ***********");
		System.out.println(" *************");
		System.out.println("      ***     ");
		System.out.println("      ***      ");
		
//Aufgabe 3

		System.out.printf( "|%.2f|\n" , 22.4234234);
		System.out.printf( "|%.2f|\n" , 111.2222);
		System.out.printf( "|%.2f|\n" , 4.0);
		System.out.printf( "|%.2f|\n" , 1000000.551);
		System.out.printf( "|%.2f|\n" , 97.34);
		
		 
	}	
}